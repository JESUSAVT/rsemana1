function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */

  var clave = txtClave.value;
  var valor = txtValor.value;

  localStorage.setItem(clave, valor);

  var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
  localStorage.setItem("json", JSON.stringify(objeto));
}
function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = localStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  alert(valor);
  //cnvertir un objeto a json
  var datosUsuario = JSON.parse(localStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}
//----------------------------------------//
function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
}

function leerDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  alert(valor);
}

function BorrarValor(){
  var clave = txtClave.value;
  var valor = localStorage.getItem(clave);
  var valorElimina = localStorage.removeItem(clave);
  alert("Clave eliminada " + valor);
}

function LimpiarDatos() {
  localStorage.clear();
  sessionStorage.clear();
  alert("Local y Session Storage Limpiada");
}
function CantidadRegistros() {
  var Tsession=sessionStorage.length;
  var Tlocal=parseInt(localStorage.length);
  var Total=Tsession+Tlocal;
  alert("Cantiad de registros en Session es "+  Tsession+ "\n"
  +"Cantidad de registros en Local es "+ Tlocal +"\n"
  +"En Total " + Total);
}

//-------------------//
